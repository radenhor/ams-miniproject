package com.example.ams.repository.provider;

import com.example.ams.model.Category;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    private final String tb_category = "tb_category";
    public String add(Category category){
        return new SQL(){{
            INSERT_INTO(tb_category);
            VALUES("name","'"+category.getCatName()+"'");
        }}.toString();
    }

    public String findAll(Integer id){
        return new SQL(){{
            SELECT("id,name");
            FROM(tb_category);
            if(id!=null){
                WHERE("id = " + id);
            }
        }}.toString();
    }

    public String deleteById(int id){
        return new SQL(){{
            DELETE_FROM(tb_category);
            WHERE("id = " + id);
        }}.toString();
    }

    public String updateById(Category category){
        return new SQL(){{
            UPDATE(tb_category);
            SET("name = '"+category.getCatName()+"'");
            WHERE("id = "+category.getId());
        }}.toString();
    }
}
