package com.example.ams.repository;

import com.example.ams.model.Article;
import com.example.ams.model.Category;
import com.example.ams.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface CategoryRepository {
    @Select("delete from tb_category where id = #{id}")
    void deleteById(int id);
    @SelectProvider(method = "findAll",type = CategoryProvider.class)
    @Results({
            @Result(property = "catName",column = "name")
    })
    List<Category> findAll();
    @SelectProvider(method = "findAll",type = CategoryProvider.class)
    @Results({
            @Result(property = "catName",column = "name")
    })
    Category findById(int id);
    @UpdateProvider(method = "updateById",type = CategoryProvider.class)
    void update(Category category);
    @InsertProvider(method = "add",type = CategoryProvider.class)
    void add(Category category);
    @Select("Select count(*) from tb_category")
    Integer allPage();
    @Select("SELECT * FROM tb_category " +
            "LIMIT 5 OFFSET #{page}")
    @Results({
            @Result(property = "catName",column = "name")
    })
    List<Category> findByPage(int page);
}
