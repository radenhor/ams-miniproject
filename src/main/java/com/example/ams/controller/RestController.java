package com.example.ams.controller;

import com.example.ams.model.Article;
import com.example.ams.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {

    private ArticleService articleService;

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping("/article")
    public Map<String,Object> findAllArticle(){
        List<Article> articles = articleService.findAll();
        Map<String,Object> map = new HashMap<>();
        map.put("STATUS",true);
        map.put("MESSAGE","YOU HAVE SUCCESSFULLY RETRIEVE ALL DATA");
        map.put("CREATED DATE",new Date().toString());
        map.put("DATA",articles);
        return map;
    }
}
