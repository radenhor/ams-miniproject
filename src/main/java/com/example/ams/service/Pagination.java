package com.example.ams.service;

import com.example.ams.controller.HomeController;
import com.example.ams.model.Article;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;

public class Pagination {

//    public static List<String> convertPage(int page){
//        char kh[] = {'០','១','២','៣','៤','៥','៦','៧','៨','៩'};
//        char en[] = {'0','1','2','3','4','5','6','7','8','9'};
//        Locale locale = LocaleContextHolder.getLocale();
//        String lang = locale.getLanguage().trim().toLowerCase();
//        List<String> list = new ArrayList<>();
//        if(lang=="en"||lang=="kr"){
//            for(int i=1;i<=page;i++){
//                list.add(""+i);
//            }
//        }else {
//            for (int i=1;i<=page;i++){
//                char[] pageSplit = String.valueOf(i).toCharArray();
//                String num = "";
//                for(int j=0;j<pageSplit.length;j++){
//                    for(int k=0;k<en.length;k++){
//                        if(pageSplit[j]==en[k]){
//                            num = num + kh[k];
//                            break;
//                        }
//                    }
//                }
//                list.add(num);
//            }
//        }
//        return list;
//    }


    public static int inc = 10;

    public static TreeMap<Integer,String> convertPage(int all, int allPage){
        char kh[] = {'០','១','២','៣','៤','៥','៦','៧','៨','៩'};
        char en[] = {'0','1','2','3','4','5','6','7','8','9'};
        TreeMap<Integer,String> map = new TreeMap<>();
        System.out.println("Start : "+HomeController.startPage);
        Locale locale = LocaleContextHolder.getLocale();
        String lang = locale.getLanguage().trim().toLowerCase();
        if(lang=="en"||lang=="kr"){
            for(int i=HomeController.startPage;i<=all;i++){
                map.put(i,""+i);
                if(i>=(allPage / 10 + (allPage % 10 == 0 ? 0 : 1))){
                    break;
                }
            }
        }else {
            for (int i=HomeController.startPage;i<=all;i++){
                char[] pageSplit = String.valueOf(i).toCharArray();
                String num = "";
                for(int j=0;j<pageSplit.length;j++){
                    for(int k=0;k<en.length;k++){
                        if(pageSplit[j]==en[k]){
                            num = num + kh[k];
                            break;
                        }
                    }
                }
                map.put(i,num);
                if(i>=(allPage / 10 + (allPage % 10 == 0 ? 0 : 1))){
                    break;
                }
            }
        }

        System.out.println("Map "+map.toString());
        return map;
    }

    public static TreeMap<Integer,String> convertCatPage(int all){
        char kh[] = {'០','១','២','៣','៤','៥','៦','៧','៨','៩'};
        char en[] = {'0','1','2','3','4','5','6','7','8','9'};
        TreeMap<Integer,String> map = new TreeMap<>();
        Locale locale = LocaleContextHolder.getLocale();
        String lang = locale.getLanguage().trim().toLowerCase();
        if(lang=="en"||lang=="kr"){
            for(int i=1;i<=all;i++){
                map.put(i,""+i);
            }
        }else {
            for (int i=1;i<=all;i++){
                char[] pageSplit = String.valueOf(i).toCharArray();
                String num = "";
                for(int j=0;j<pageSplit.length;j++){
                    for(int k=0;k<en.length;k++){
                        if(pageSplit[j]==en[k]){
                            num = num + kh[k];
                            break;
                        }
                    }
                }
                map.put(i,num);
            }
        }
        return map;
    }

    public static List<String> convertPage1(int all,int allPage){
        char kh[] = {'០','១','២','៣','៤','៥','៦','៧','៨','៩'};
        char en[] = {'0','1','2','3','4','5','6','7','8','9'};
        Locale locale = LocaleContextHolder.getLocale();
        String lang = locale.getLanguage().trim().toLowerCase();
        List<String> list = new ArrayList<>();
//        System.out.println("START"+HomeController.startPage);
        if(lang=="en"||lang=="kr"){
            for(int i=HomeController.startPage;i<=all;i++){
                list.add(""+i);
                if(i>=(allPage / 10 + (allPage % 10 == 0 ? 0 : 1))){
                    break;
                }
            }
        }else {
            for (int i=HomeController.startPage;i<=all;i++){
                char[] pageSplit = String.valueOf(i).toCharArray();
                String num = "";
                for(int j=0;j<pageSplit.length;j++){
                    for(int k=0;k<en.length;k++){
                        if(pageSplit[j]==en[k]){
                            num = num + kh[k];
                            break;
                        }
                    }
                }
                list.add(num);
            }
        }
        return list;
    }

    public static Integer preparePage(Integer allRecord,int spage){
        int allPage = (allRecord / 10 + (allRecord % 10 == 0 ? 0 : 1));
        if(allRecord>=spage){
            if(allRecord>10){
                if(HomeController.startPage==HomeController.page){
                    HomeController.startPage = inc-9;
                    if(HomeController.startPage<0){
                        HomeController.startPage = 1;
                    }
                    inc -= 5;
                    if(inc<10){
                        inc = 10;
                    }
                }
                if(HomeController.page>=inc&&HomeController.page<inc+5){
                    HomeController.startPage = inc-4;
//                    System.out.println("Before"+inc);
                    inc=inc+5;
//                    System.out.println("After"+inc);
                }else {
                    HomeController.startPage = inc-9;
                    if(HomeController.startPage<0){
                        HomeController.startPage = 1;
                    }
                }
                return inc>allPage?allPage:inc;
            }
            return allPage;
        }else {
            return 1;
        }
    }

    public static Integer prePageCat(int allRecord){
        if(allRecord>5){
            return  (allRecord / 5 + (allRecord % 5 == 0 ? 0 : 1));
        }
        return 1;
    }

    public static Integer pageToIndex(String page,int all,int allPage){
        List<String> list = new ArrayList<>();
        for(int i=HomeController.startPage;i<=all;i++){
            list.add(""+i);
            if(i>=(allPage / 10 + (allPage % 10 == 0 ? 0 : 1))){
                break;
            }
        }
        System.out.println("PAGE"+list.toString() );
        for(int i=0;i<list.size();i++){
            if(list.get(i).equals(page)){
//                System.out.println("INDEX"+i);
                return i;
            }
        }
        return 0;
    }


}
