package com.example.ams.model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.InputStream;

public class Article {

    @Nullable
    private String createdDate;
    @Nullable
    private int id;
    @NotBlank(message = "* Field cannot be blank")
    @Size(min = 3,max = 50,message = "* Field must be > 3 and < 50")
    private String title;
    @NotBlank(message = "* Field cannot be blank")
    @Size(min = 3,max = 50,message = "* Field must be > 3 and < 50")
    private String author;
    @NotBlank(message = "* Field cannot be blank")
    @Size(min = 3,max = 50,message = "* Field must be > 3 and < 50")
    private String description;
    @NotBlank(message = "* Field cannot be blank")
    private String image;
    private Category category;


    public Article() {

    }

    public Article(@Nullable String createdDate, int id, @NotBlank(message = "* Field cannot be blank") @Size(min = 3, max = 50, message = "* Field must be > 3 and < 50") String title, @NotBlank(message = "* Field cannot be blank") @Size(min = 3, max = 50, message = "* Field must be > 3 and < 50") String author, @NotBlank(message = "* Field cannot be blank") @Size(min = 3, max = 50, message = "* Field must be > 3 and < 50") String description, @NotBlank(message = "* Field cannot be blank") String image, Category category) {
        this.createdDate = createdDate;
        this.id = id;
        this.title = title;
        this.author = author;
        this.description = description;
        this.image = image;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Article{" +
                "createdDate='" + createdDate + '\'' +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", category=" + category +
                '}';
    }
}
