package com.example.ams.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfiguration {

    @Bean
    @Profile("production")
    public DataSource dataSource(){
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
        driverManagerDataSource.setUrl("jdbc:postgresql://localhost:5432/dvdrental");
        driverManagerDataSource.setUsername("postgres");
        driverManagerDataSource.setPassword("181099");
        return driverManagerDataSource;
    }
    @Bean
    @Profile("development")
    public DataSource development(){
//        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
//        driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
//        driverManagerDataSource.setUrl("jdbc:postgresql://localhost:5432/development");
//        driverManagerDataSource.setUsername("postgres");
//        driverManagerDataSource.setPassword("181099");
//        return driverManagerDataSource;
//        EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder();
//        embeddedDatabaseBuilder.setType(EmbeddedDatabaseType.H2);
//        embeddedDatabaseBuilder.addScript("sql/table.sql");

        EmbeddedDatabaseBuilder embeddedDatabaseBuilder1 = new EmbeddedDatabaseBuilder();
        embeddedDatabaseBuilder1.setType(EmbeddedDatabaseType.H2);
        embeddedDatabaseBuilder1.addScript("sql/table.sql");
        embeddedDatabaseBuilder1.addScript("sql/insert.sql");
        return  embeddedDatabaseBuilder1.build();
    }
}
