package com.example.ams.service;

import com.example.ams.model.Article;

import java.util.List;
import java.util.Set;

public interface ArticleService {
    List<Article> findAll();
    Article findById(int id);
    void deleteById(int id);
    void add(Article article);
    void update(Article article);
    Integer filterCategory(int catId);
    List<Article> filterCategoryPage(int cat_id,int page);
    List<Article> findByPage(int page);
    Integer allPage();
    Integer filterByTitle(String title,int catId);
    List<Article> filterByTitlePage(int cat_id,String title,int page);
    Integer filterAllArticle(String title);
    List<Article> filterAllArticleByPage(String title,int page);
}
