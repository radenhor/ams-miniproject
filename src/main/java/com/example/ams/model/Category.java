package com.example.ams.model;

import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Category {

    @Nullable
    private int id;
    @NotBlank(message = "* Field cannot be blank")
    @Size(min = 3,max = 50,message = "* Field must be > 3 and < 50")
    private String catName;

    public Category() { }

    public Category(String catName) {
        this.catName = catName;
    }

    public Category(int id) {
        this.id = id;
    }

    public Category(int id, String catName) {
        this.id = id;
        this.catName = catName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", catName='" + catName + '\'' +
                '}';
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }
}
