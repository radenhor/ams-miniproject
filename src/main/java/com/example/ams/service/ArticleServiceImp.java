package com.example.ams.service;

import com.example.ams.controller.HomeController;
import com.example.ams.model.Article;
import com.example.ams.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class ArticleServiceImp implements ArticleService{

    ArticleRepository articleRepository;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> findAll() {

        List<Article> list = articleRepository.findAll();
        for(int i=0;i<list.size();i++){
            if(list.get(i).getImage()==null){
                list.get(i).setImage("cncc.png");
            }
        }
        return list;
    }

    @Override
    public Article findById(int id) {
        Article article = articleRepository.findById(id);
            if(article.getImage()==null) {
                article.setImage("cncc.png");
            }
        return article;
    }

    @Override
    public void deleteById(int id) {

        articleRepository.deleteById(id);
        int allPage,page;
        if(HomeController.cat_id!=0){
           allPage = articleRepository.filterCategory(HomeController.cat_id);
           page = allPage / 10 + (allPage%10 == 0 ? 0 : 1);
        }else {
            allPage = articleRepository.allPage();
            page = allPage / 10 + (allPage%10 == 0 ? 0 : 1);
        }

        if((allPage%10)<=0&&page == HomeController.page-1)HomeController.page--;
        System.out.println("PAGE : "+HomeController.page);
    }

    @Override
    public void add(Article article) {

        articleRepository.add(article);
        HomeController.page = articleRepository.allPage()/10 + (articleRepository.allPage()%10==0?0:1);
    }

    @Override
    public void update(Article article) {
        articleRepository.update(article);
    }

    @Override
    public Integer filterCategory(int catId) {
        return articleRepository.filterCategory(catId);
    }

    @Override
    public List<Article> findByPage(int page) {
        return articleRepository.findByPage((page-1)*10);
    }

    @Override
    public List<Article> filterCategoryPage(int cat_id, int page) {
        return articleRepository.filterCategoryPage(cat_id,(page-1)*10);
    }

    @Override
    public Integer allPage() {
        return articleRepository.allPage();
    }

    @Override
    public Integer filterByTitle(String title, int catId) {
        return articleRepository.filterByTitle(title,catId);
    }

    @Override
    public List<Article> filterByTitlePage(int cat_id,String title,int page) {
        return articleRepository.filterByTitlePage(cat_id,title,(page-1)*10);
    }

    @Override
    public Integer filterAllArticle(String title) {
        return articleRepository.filterAllArticle(title);
    }

    @Override
    public List<Article> filterAllArticleByPage(String title, int page) {
        return articleRepository.filterAllArticleByPage(title,(page-1)*10);
    }
}
